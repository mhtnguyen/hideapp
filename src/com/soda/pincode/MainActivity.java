package com.soda.pincode;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.soda.pincode.Shell.ShellException;

public class MainActivity extends Activity {
	private ListView lstTest;
	private AppsAdapter appsAdapter;
	private static Context context = null;
	private ArrayList<Apps> apps = null;
	private File tempBuildProp;
	private String m_Text = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// Initialize ListView
		lstTest = (ListView) findViewById(R.id.lstText);
		context = this;

		// Initialize our ArrayList
		apps = getInstalledApps(true);
		// for (Users u : users) {
		// System.out.println("User id: " + u.getUserid1() + " name: "
		// + u.getUsername() + " id2: " + u.getUserid2());
		// }
		// Initialize our array adapter notice how it references the
		// listitems.xml layout
		appsAdapter = new AppsAdapter(this, R.layout.list_item, apps);
		// Set the above adapter as the adapter of choice for our list
		lstTest.setAdapter(appsAdapter);
		lstTest.setTextFilterEnabled(true);

		lstTest.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// When clicked, show a toast with the TextView text
				final Apps clickedObject = (Apps) appsAdapter.getItem(position);
				// Toast.makeText(getBaseContext(), clickedObject.getAppName(),
				// Toast.LENGTH_SHORT).show();

				// get prompts.xml view
				LayoutInflater li = LayoutInflater.from(context);
				View promptsView = li.inflate(R.layout.prompt, null);
				// input prompt
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						context);

				// set prompts.xml to alertdialog builder
				alertDialogBuilder.setView(promptsView);

				final EditText userInput = (EditText) promptsView
						.findViewById(R.id.editTextDialogUserInput);

				// set dialog message
				alertDialogBuilder
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										// get user input and set it to result
										// edit text
										String passcode = userInput.getText()
												.toString();
										System.out.println("packagename: "
												+ clickedObject
														.getPackageName()
												+ " pincode: " + passcode);
										SharedPreferences pref = getSharedPreferences(
												"pinstore",
												MODE_WORLD_READABLE);
										Editor editor = pref.edit();
										editor.putString(passcode,
												clickedObject.getPackageName());
										try {
											System.out.println("Returned: "+editor.commit());
										}catch(Exception e){
											System.out.println("Failed to Write to disk: "+e);
										}
										
										// Toast.makeText(getApplicationContext(),
										// "Shortcut saved " +
										// clickedObject.getPackageName()+ " "+
										// passcode, Toast.LENGTH_SHORT)
										// .show();

										// XSharedPreferences sp = new
										// XSharedPreferences("com.soda.pincode");
										// Editor editor = sp.edit();
										// editor.putString(passcode,
										// clickedObject.getPackageName());
										// editor.apply();
										if (Shell.su()) {
											try {
												String output = Shell.sudo("pm disable "
														+ clickedObject
																.getPackageName()
														+ "\n");
											} catch (ShellException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}

										}

										String pname_to_launch = pref
												.getString(passcode, "Empty");
										Toast.makeText(getApplicationContext(),
												"Retrieved " + pname_to_launch,
												Toast.LENGTH_SHORT).show();

									}

								})
						.setNegativeButton("Cancel",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});

				// create alert dialog
				AlertDialog alertDialog = alertDialogBuilder.create();

				// show it
				alertDialog.show();

				// switchProfile(clickedObject.getUserid1());
			}

		});

	}

	public ArrayList<Apps> getInstalledApps(boolean getSysPackages) {
		ArrayList<Apps> res = new ArrayList<Apps>();
		PackageManager pm = getPackageManager();
		List<PackageInfo> packs = pm.getInstalledPackages(0);
		for (int i = 0; i < packs.size(); i++) {
			PackageInfo p = packs.get(i);
			Apps newInfo = new Apps();
			ApplicationInfo ai;
			try {
				ai = pm.getApplicationInfo(p.packageName, 0);
				if((ai.flags & ApplicationInfo.FLAG_SYSTEM) == 0){
				System.out.println(ai);
				newInfo.setAppName(p.applicationInfo.loadLabel(getPackageManager())
						.toString());
				newInfo.setPackageName(p.packageName);
				res.add(newInfo);
				}
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
		}
		return res;
	}

	public void addProfiles(View v) {
		try {
			fillValues();
			commit();
			finish();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"Failed enabling User Profiles." + e, Toast.LENGTH_SHORT)
					.show();

		}
	}

	public void switchProfile(int i) {
		try {
			if (Shell.su()) {
				String output = Shell.sudo("am switch-user " + i + "\n");
			}
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"Failed Switching Profiles." + e, Toast.LENGTH_SHORT)
					.show();

		}
	}

	public void addNewUser(String username) {
		try {
			if (Shell.su()) {
				String output = Shell.sudo("pm create-user " + username + "\n");
				Toast.makeText(getApplicationContext(), output,
						Toast.LENGTH_SHORT).show();
				appsAdapter.notifyDataSetChanged();
				lstTest.invalidateViews();
			}

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"Failed Adding New user." + e, Toast.LENGTH_SHORT).show();

		}
	}

	// private ArrayList<Users> getUserList(Context context) {
	// ArrayList<Users> userList = new ArrayList<Users>();
	// Pattern pattern = Pattern.compile("(?<=\\{)([^}]*)(?=\\})");
	//
	// try {
	//
	// if (Shell.su()) {
	// String output = Shell.sudo("pm list users\n");
	// // System.out.println(output);
	// Matcher m = pattern.matcher(output);
	// while (m.find()) {
	// String[] parts = m.group().split(":");
	// if (parts != null && parts.length == 3) {
	// userList.add(new Users(parts[0], parts[1], parts[2]));
	// } else {
	// System.out.println("Failed Parsing User List");
	// }
	// }
	// // for(Users u : userList) {
	// // System.out.println("User id: " + u.getUserid1() + " name: " +
	// // u.getUsername() + " id2: " + u.getUserid2());
	// // }
	// }
	//
	// } catch (Exception e) {
	// Toast.makeText(getApplicationContext(),
	// "Unable to get user list." + e, Toast.LENGTH_SHORT).show();
	// }
	// return userList;
	//
	// }

	private void backupBuildProp() {
		try {
			// Preform su to get root privledges
			Process p = Runtime.getRuntime().exec("su");

			// Attempt to write a file to a root-only
			DataOutputStream os = new DataOutputStream(p.getOutputStream());
			// Remounting /system as read+write
			os.writeBytes("mount -o rw,remount /system\n");
			// Copying file to SD Card
			os.writeBytes("cp -f /system/build.prop "
					+ Environment.getExternalStorageDirectory()
							.getAbsolutePath() + "/build.prop.bak\n");
			os.writeBytes("cp -f /system/build.prop "
					+ Environment.getExternalStorageDirectory()
							.getAbsolutePath() + "/build.prop.temp\n");
			os.writeBytes("exit\n");
			os.flush();
			p.waitFor();

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(),
					"Unable to get root access. Please restart the app." + e,
					Toast.LENGTH_SHORT).show();

		}

	}

	private void fillValues() {

		backupBuildProp();
		final Properties properties = new Properties();
		tempBuildProp = new File(Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/build.prop.temp");

		try {
			properties.load(new FileInputStream(tempBuildProp));
		} catch (IOException e) {
			Toast.makeText(getApplicationContext(), "Error: " + e,
					Toast.LENGTH_SHORT).show();

		}

		final String[] pTitle = properties.keySet().toArray(new String[0]);
		final ArrayList<String> pDesc = new ArrayList<String>();
		for (int i = 0; i < pTitle.length; i++) {
			pDesc.add(properties.getProperty(pTitle[i]));
			System.out.println("Property : " + pTitle[i] + "  Value : "
					+ pDesc.get(i));
		}

	}

	public void commit() {

		final Properties properties = new Properties();
		try {

			FileInputStream in = new FileInputStream(tempBuildProp);
			properties.load(in);
			in.close();
		} catch (IOException e) {
			Toast.makeText(getApplicationContext(),
					"Error loading properties: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}

		properties.setProperty("fw.max_users", "99");

		try {
			FileOutputStream changedOutput = new FileOutputStream(tempBuildProp);
			properties.store(changedOutput, null);
			changedOutput.close();

			copyToSystem();
		} catch (IOException e) {
			Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}

	}

	private void restore() {
		Process process;
		DataOutputStream os;
		try {
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes("mount -o rw,remount /system\n");
			os.writeBytes("busybox cp -f "
					+ Environment.getExternalStorageDirectory()
							.getAbsolutePath() + "/build.prop.bak"
					+ " /system/build.prop\n");
			os.writeBytes("chmod 755 /system/build.prop\n");
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
			Toast.makeText(getApplicationContext(),
					"Original build.prop restored !", Toast.LENGTH_SHORT)
					.show();

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}
	}

	private void copyToSystem() {
		Process process = null;
		DataOutputStream os = null;

		try {
			process = Runtime.getRuntime().exec("su");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes("mount -o rw,remount /system\n");
			os.writeBytes("mv -f /system/build.prop /system/build.prop.bak\n");
			os.writeBytes("busybox cp -f " + tempBuildProp
					+ " /system/build.prop\n");
			os.writeBytes("chmod 755 /system/build.prop\n");
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
			Toast.makeText(
					getApplicationContext(),
					"Your changes have been comitted. Please restart the phone to observe the changes!",
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(),
					Toast.LENGTH_SHORT).show();
			restore();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				process.destroy();
			} catch (Exception e) {
				Toast.makeText(getApplicationContext(),
						"Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}
	}

}
