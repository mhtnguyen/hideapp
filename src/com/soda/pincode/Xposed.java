package com.soda.pincode;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit.StartupParam;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XC_MethodHook.MethodHookParam;
import de.robv.android.xposed.XSharedPreferences;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;

public class Xposed implements IXposedHookLoadPackage {

	private XSharedPreferences pref;
	private Context c = null;
	private String password = "";

	@Override
	public void handleLoadPackage(final LoadPackageParam lpparam)
			throws Throwable {
		
		if (!lpparam.packageName.equals("com.android.systemui"))
			return;
		
		XposedHelpers.findAndHookMethod(
				"com.android.internal.widget.LockPatternUtils",
				lpparam.classLoader, "checkPassword", String.class,
				new XC_MethodHook() {

					// need to get password before screen is gone.
					@Override
					protected void beforeHookedMethod(MethodHookParam param)
							throws Throwable {

						password = (String) param.args[0];
						log("pin entered: " + password);

					}

					@Override
					protected void afterHookedMethod(MethodHookParam param)
							throws Throwable {

						File file = new File(
								"/data/data/com.soda.pincode/shared_prefs/pinstore.xml");
						pref = new XSharedPreferences(file);

						/*
						 * Map<String, ?> spValues = pref.getAll();
						 * 
						 * for (String key: spValues.keySet()) {
						 * 
						 * log("key : " + key); log("value : " +
						 * spValues.get(key)); }
						 */
						final String packageName = pref.getString(password,
								"default");
						
						log("Retrieved from Xposed: " + packageName);
						if (!packageName.equalsIgnoreCase("default")) {
							log("asynch task: " + packageName);
							AsyncTaskRunner runner = new AsyncTaskRunner();
							runner.execute(packageName);
							c = (Context) XposedHelpers.getObjectField(
									param.thisObject, "mContext");
							Intent intent = getIntent(packageName, c);
							//wait for app to be enabled and then launch
							PackageManager manager = c.getPackageManager();
							ApplicationInfo ai =manager.getApplicationInfo(packageName,0);
							boolean appStatus = ai.enabled;
							int counter=0;
							while(!appStatus){
								if(counter>=25){
									break;
								}
								else
									Thread.sleep(250);
							}
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							c.startActivity(intent);
							

						}

					}

				});

	}

	public Intent getIntent(String pname, Context c) {
		Intent i = null;
		PackageManager manager = c.getPackageManager();
		try {
			
			i = manager.getLaunchIntentForPackage(pname);
			if (i == null)
				throw new PackageManager.NameNotFoundException();
			i.addCategory(Intent.CATEGORY_LAUNCHER);
		} catch (PackageManager.NameNotFoundException e) {
			Xposed.log("PackageManager.NameNotFoundException: "+e);
		}
		return i;
	}

	public void initZygote(StartupParam startupParam) throws Throwable {
		pref = new XSharedPreferences("com.soda.pincode", "pinstore");
	}

	public static void log(String string) {
		XposedBridge.log(string);
	}
}

class AsyncTaskRunner extends AsyncTask<String, String, String> {
	private Context c = null;
	private String resp;
	private String packageName="";
	@Override
	protected String doInBackground(String... params) {
		Process process = null;
		DataOutputStream os = null;
		try {
			// Do your long operations here and return the result
			packageName = params[0];
			Xposed.log("inside async: " + packageName);
			
			//samsung knox needs to be disabled
			//super su needs to be installed and android ui granted su
			process = Runtime.getRuntime().exec("su");
			Xposed.log("Got su ");
			os = new DataOutputStream(process.getOutputStream());
			os.writeBytes("pm enable " + packageName + "\n");
			Xposed.log("pm enable ");
			os.writeBytes("exit\n");
			os.flush();
			Xposed.log("flush ");
		    String line;
			BufferedReader reader =
					new BufferedReader(new InputStreamReader(process.getInputStream()));
					while ((line = reader.readLine()) != null) {Xposed.log("line: "+line);}
		    int exitCode = process.waitFor();
			Xposed.log("waitfor: "+exitCode);
			Xposed.log("after async: " + packageName);
			

		} catch (Exception e) {
			Xposed.log("error: " + e);
			resp = e.getMessage();
		}
		return resp;
	}


	public Intent getIntent(String pname, Context c) {
		Intent i = null;
		PackageManager manager = c.getPackageManager();
		try {
			i = manager.getLaunchIntentForPackage(pname);
			if (i == null)
				throw new PackageManager.NameNotFoundException();
			i.addCategory(Intent.CATEGORY_LAUNCHER);
		} catch (PackageManager.NameNotFoundException e) {

		}
		return i;
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	protected void onPostExecute(MethodHookParam param) {
		// execution of result of Long time consuming operation
		
		//****** launch app here but can't pass Xposed param?
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override
	protected void onPreExecute() {
		// Things to be done before execution of long running operation. For
		// example showing ProgessDialog
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.os.AsyncTask#onProgressUpdate(Progress[])
	 */
	@Override
	protected void onProgressUpdate(String... text) {
		// Things to be done while execution of long running operation is in
		// progress. For example updating ProgessDialog
	}
}
