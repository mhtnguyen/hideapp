package com.soda.pincode;

public class Apps {
	private String packageName;
    private String appName;
   

    public String getAppName() {
        return appName;
    }

    public void setAppName(String name) {
        this.appName = name;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String name) {
        this.packageName = name;
    }


    public Apps() {

    }

    public Apps(String appName, String packageName) {
        try {
            this.appName = appName;
            this.packageName = packageName;
         
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
