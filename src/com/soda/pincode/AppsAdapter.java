package com.soda.pincode;

import java.util.ArrayList;
import java.util.List;

import com.soda.pincode.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppsAdapter extends ArrayAdapter<Apps>{
	
	 int resource;
	    String response;
	    Context context;
	    //Initialize adapter
	    public AppsAdapter(Context context, int resource, ArrayList<Apps> apps) {
	        super(context, resource, apps);
	        this.resource=resource;
	 
	    }
	     
	   
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent)
	    {
	        LinearLayout appsView;
	        //Get the current alert object
	        Apps al = getItem(position);
	         
	        //Inflate the view
	        if(convertView==null)
	        {
	            appsView = new LinearLayout(getContext());
	            String inflater = Context.LAYOUT_INFLATER_SERVICE;
	            LayoutInflater vi;
	            vi = (LayoutInflater)getContext().getSystemService(inflater);
	            vi.inflate(resource, appsView, true);
	        }
	        else
	        {
	            appsView = (LinearLayout) convertView;
	        }
	        //Get the text boxes from the listitem.xml file
	        TextView appName =(TextView)appsView.findViewById(R.id.appName);
	        TextView packageName =(TextView)appsView.findViewById(R.id.packageName);
	         
	        //Assign the appropriate data from our alert object above
	        appName.setText(al.getAppName());
	        packageName.setText(al.getPackageName());
	         
	        return appsView;
	    }

}
